#include <iostream>
using namespace std;

class Cat
{
    public:
        void sayHello()
        {
            cout << "Meow" <<endl;
        }
};


int main(){
   Cat myCat;
   myCat.sayHello();
   return 0;
}
